package org.gameofapps.s05demo01.demo_recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // The data model for the recycler view
    // --an array of Person objects
    ArrayList<Person> persons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Populate (add items into) the persons array
        persons.add(new Person("John", "Smith"));
        persons.add(new Person("Jane", "Doe"));

        // Connect recyclerView in XML layout to our Java code
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);

        // Create a layout manager for the recyclerView
        // This one is a linearLayoutManager, so it arranges all its
        // elements in a linear fashion (in a column)
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        // Tell the recyclerView what layout manager it needs to use
        // Since we're giving it a linear layout manager, then the
        // recyclerView will arrange all its elements in a linear column
        recyclerView.setLayoutManager(linearLayoutManager);

        // Create a PersonAdapter object and give it the data model we created above
        PersonsAdapter personsAdapter = new PersonsAdapter(persons);

        // Tell the recyclerView to use this personAdapter object as its adapter
        recyclerView.setAdapter(personsAdapter);
    }
}