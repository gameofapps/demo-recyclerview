package org.gameofapps.s05demo01.demo_recyclerview;

public class Person {

    // Public properties
    String firstName;
    String lastName;

    // Constructor
    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
