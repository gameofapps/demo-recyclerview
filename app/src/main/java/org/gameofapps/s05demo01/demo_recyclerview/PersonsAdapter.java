package org.gameofapps.s05demo01.demo_recyclerview;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonViewHolder> {

    // Methods for providing information back to RecyclerView so it knows what and how
    // to display the items
    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a layout inflater object (use it to inflate or load the XML file for
        // the item layout)
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // Inflate (load from XML) the person item's layout
        View itemView = layoutInflater.inflate(R.layout.item_layout_person, parent, false);

        // Create a view holder (PersonViewHolder) that is connected to this item layout
        PersonViewHolder viewHolder = new PersonViewHolder(itemView);

        // Return this view holder
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        // Get the person item corresponding to this position
        Person person = persons.get(position);

        // Set the values of the first/last names of the item to the first/last
        // names of the corresponding Person object
        holder.firstNameTextView.setText(person.firstName);
        holder.lastNameTextView.setText(person.lastName);
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    // Constructor
    PersonsAdapter(ArrayList<Person> persons) {
        this.persons = persons;
    }

    // Private property to hold the data model (array of persons)
    private ArrayList<Person> persons = null;

    // ViewHolder class for Person items
    protected class PersonViewHolder extends RecyclerView.ViewHolder {

        // Properties for connecting to text views in item layout
        private TextView firstNameTextView;
        private TextView lastNameTextView;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);

            // Connect views from item layout XML to Java properties
            firstNameTextView = itemView.findViewById(R.id.first_name_text_view);
            lastNameTextView = itemView.findViewById(R.id.last_name_text_view);
        }
    }
}
